# Coding Challenge

Welcome! This is my personal coding challenge to anyone who would like to try :) 

There'll be more challenges as I upload more interesting problems to this page.

## Challenge 1 - TTT Game
Build a TicTacToe game. If you've never heard of TicTacToe, the game is played on a 3x3 board. There are 2 players. The goal of the game is to have 3 of the same in a row, either horizontally or vertically or diagonally.

```
| X | O | X |
| O | O | X |
| X | X | O |
```

### The focus of this challenge is: 
1. Correctness - this is critical to the game logic. The game should work and function as described
2. Structure of the code - code should be clean, uses coding best practices, and ideally, some design patterns ;) 

### What is expected
- should have a **README** on how to run
- a gif of how the game runs. I recommend using something like [this app](https://www.cockos.com/licecap/) to make a gif


## Challenge 2 - Twitter Client
Write your own Twitter client! There are multiple Twitter clients out there, but I want to get one that's specifically does two things: 

1. Supports a `getMutualFriends` feature that takes in two user IDs and finds the mutual friends between them. Say user A has friends B, C, D and user K has friends B, C, F, `getMutualFriends` for A and K will return B and C.
2. Supports a `getExclusiveFriends` feature that takes in two user IDs and finds the exclusive friends of the two users. Say user A has friends B, C, D and user K has friends B, C, F, `getMutualFriends` for A and K will return D and F.


### The focus of this challenge is: 
1. Correctness - this is critical because the API should work correctly and for __any scale__. If not, then should explain why.
2. Structure of the code - code should be clean, uses coding best practices, and ideally, some design patterns ;) 
3. How do you use the API is critical.

### What is expected
- should have a **README** on how to run
- a gif of your client running. I recommend using something like [this app](https://www.cockos.com/licecap/) to make a gif

